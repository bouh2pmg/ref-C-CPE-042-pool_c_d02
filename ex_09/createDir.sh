#!/bin/bash
i=$1
n=1
while (($i > 0))
do
    printf -v format "%02d" $n
    mkdir -p ex_$format
    i=$((i-1))
    n=$((n+1))
done
